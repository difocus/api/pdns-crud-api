<?php
require '../vendor/autoload.php';

use Monolog\ErrorHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use ShopExpress\PowerDnsCrudApi\ApiClient as PowerDnsApiClient;

$config = parse_ini_file(__DIR__ . '/.env');

//-------------- конфиг
$domain = 'shopexpress.site';
$subDomains = [];
$ip = '136.243.7.38';
$dkim = null; // флаг если записи для рассыльщика
//-------------- /конфиг

$logger = new Logger($domain);
$handler = new RotatingFileHandler("{$domain}.log", 5, Logger::DEBUG);
$logger->pushHandler($handler);

ErrorHandler::register($logger);

$r = new PowerDnsApiClient($config['BASE_URL'], $config['API_TOKEN']);
$r->setLogger($logger);

$isDomain = empty($subDomains); // флаг если домен или поддомен
$isEmail = (bool)$dkim;

try {
    $d2 = $r->getDomain($domain);
} catch (Exception $exception) {
    $d1 = $r->addDomain(['name' => $domain, 'type' => 'MASTER']);
    $d2 = $r->getDomain($domain)
        ->addRecord(['name' => $domain, 'type' => PowerDnsApiClient::SOA, 'content' => 'ns1.difocus.ru hostmaster.difocus.ru ' . date('Ymd') . '00 28800 7200 604800 86400'])
        ->addRecord(['name' => $domain, 'type' => PowerDnsApiClient::NS, 'content' => 'ns1.difocus.ru'])
        ->addRecord(['name' => $domain, 'type' => PowerDnsApiClient::NS, 'content' => 'ns2.difocus.ru'])
        ->addRecord(['name' => $domain, 'type' => PowerDnsApiClient::A, 'content' => $ip])
        ->addRecord(['name' => '*.' . $domain, 'type' => PowerDnsApiClient::A, 'content' => $ip])
        ->addRecord(['name' => 'www.' . $domain, 'type' => PowerDnsApiClient::A, 'content' => $ip])
        ->addRecord(['name' => '@.' . $domain, 'type' => PowerDnsApiClient::SPF, 'content' => "v=spf1 +a +mx include:_spf.google.com include:mailgun.org ~all"])
        ->addRecordMx(['name' => $domain, 'content' => 'mx.yandex.ru', 'prio' => 10]);
}

if ($isEmail) {
    /* для домена */
    $d1 = $r->addDomain(['name' => $domain, 'type' => 'MASTER']);
    $d2 = $r->getDomain($domain)
        //->addRecord(['name' => '@', 'type' => PowerDnsApiClient::TXT, 'content' => 'v=spf1 +a +mx include:mxsmtp.sendpulse.com include:_spf.google.com include:mailgun.org include:spf.unisender.com ~all'])
        ->addRecord(['name' => 'us._domainkey', 'type' => PowerDnsApiClient::TXT, 'content' => 'k=rsa; p=' . $dkim]);

    return true;
}

if (!empty($subDomains)) {
    /* для поддомена */
    foreach ($subDomains as $sub) {
        $d2 = $r->getDomain($domain)
            ->addRecord(
                [
                    'name' => "{$sub}.{$domain}",
                    'type' => PowerDnsApiClient::A,
                    'content' => $ip,
                    "ttl" => "86400",
                    "prio" => "0",
                    "change_date" => "1478896916",
                    "disabled" => "0",
                    "ordername" => "{$sub}.{$domain}",
                    "auth" => "1",
                ]
            );
    }

    return true;
}

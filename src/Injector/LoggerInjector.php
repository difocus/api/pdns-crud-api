<?php
namespace ShopExpress\PowerDnsCrudApi\Injector;

use Psr\Log\NullLogger;
use Psr\Log\LoggerInterface;

/**
 * Trait LoggerInjector
 * @package ShopExpress\PowerDnsCrudApi\Injector
 */
trait LoggerInjector
{
    /**
     * @var
     */
    private $logger;

    /**
     * @return NullLogger
     */
    public function getLogger()
    {
        if (!$this->logger) {
            $this->logger = new NullLogger();
        }
        return $this->logger;
    }

    /**
     * @param LoggerInterface $logger
     *
     * @return $this
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
        return $this;
    }
}

<?php
namespace ShopExpress\PowerDnsCrudApi;

use ShopExpress\PowerDnsCrudApi\Entity\Domain;
use ShopExpress\PowerDnsCrudApi\Injector\LoggerInjector;

/**
 * Class ApiClient
 * @package ShopExpress\PowerDnsCrudApi
 */
class ApiClient
{
    use LoggerInjector;

    const SOA = 'SOA';
    const A = 'A';
    const CNAME = 'CNAME';
    const MX = 'MX';
    const NS = 'NS';
    const SPF = 'SPF';
    const TXT = 'TXT';

    /**
     * @var string
     */
    private $baseUrl;
    /**
     * @var string
     */
    private $apiToken;

    /**
     * @param string $baseUrl The base url
     * @param string $apiToken The api token
     */
    public function __construct($baseUrl, $apiToken)
    {
        $this->baseUrl = trim($baseUrl, '/');
        $this->apiToken = $apiToken;
    }

    /**
     * Gets the Domain.
     *
     * @param string $domainName The Domain name
     *
     * @return Domain The Domain entity.
     * @throws \Exception
     */
    public function getDomain($domainName)
    {
        $domain = new Domain($this);

        return $domain->getByName($domainName);
    }

    /**
     * Adds a domain.
     *
     * @param array $data The data
     *
     * @return Domain The Domain entity.
     * @throws \Exception
     */
    public function addDomain(array $data)
    {
        $domain = new Domain($this, $data);

        return $domain->save();
    }

    /**
     * Adds a RecordMx.
     *
     * @param array $data The data
     *
     * @return self
     * @throws \Exception
     */
    public function addRecordMx(array $data)
    {
        $data['type'] = 'MX';

        return $this->addRecord($data);
    }

    /**
     * Adds a Record.
     *
     * @param array $data The data
     *
     * @throws \Exception
     *
     * @return self
     */
    public function addRecord(array $data)
    {
        if (!isset($data['domain_id'])) {
            throw new \Exception("Error create Record: `domain_id` field isn't set", 400);
        }

        $response = $this->makeRequest("{$this->baseUrl}/records", 'POST', $data);

        if (!isset($response['success'])) {
            throw new \Exception($response['error']['message'], 400);
        }

        $this->getLogger()->info('Record successfully added!', [$response, $data]);

        return $this;
    }

    /**
     * @param $id
     * @param array $data
     *
     * @throws \Exception
     * @return $this
     */
    public function updateRecord($id, array $data)
    {
        $response = $this->makeRequest("{$this->baseUrl}/records/{$id}", 'PATCH', $data);

        if (!isset($response['success'])) {
            throw new \Exception($response['error']['message'], 400);
        }

        $this->getLogger()->info('Record successfully updated!', [$response, $data]);

        return $this;
    }

    /**
     * @param $id
     *
     * @throws \Exception
     * @return $this
     */
    public function deleteRecord($id)
    {
        $response = $this->makeRequest("{$this->baseUrl}/records/{$id}", 'DELETE');

        if (!isset($response['success'])) {
            throw new \Exception($response['error']['message'], 400);
        }

        $this->getLogger()->info('Record successfully deleted!', [$response]);

        return $this;
    }

    /**
     * Gets the base url.
     *
     * @return string The base url.
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * Makes a request.
     *
     * @param string $objectUrl The object url
     * @param string $method The method
     * @param array $data The data
     *
     * @throws \Exception
     *
     * @return array
     */
    public function makeRequest($objectUrl, $method, $data = [])
    {
        $ch = curl_init();

        $opts = [
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_USERAGENT => 'dicms-api-php-beta-0.2',
        ];
        $opts[CURLOPT_URL] = $objectUrl . '?token=' . $this->apiToken;

        switch ($method) {
            case 'GET':
                break;
            case 'POST':
                $opts[CURLOPT_CUSTOMREQUEST] = 'POST';
                $opts[CURLOPT_RETURNTRANSFER] = true;
                $opts[CURLOPT_POSTFIELDS] = http_build_query($data);
                $opts[CURLOPT_HTTPHEADER][] = 'Content-Length: ' . strlen($opts[CURLOPT_POSTFIELDS]);
                break;
            case 'PUT':
                $opts[CURLOPT_CUSTOMREQUEST] = 'PUT';
                $opts[CURLOPT_RETURNTRANSFER] = true;
                $opts[CURLOPT_POSTFIELDS] = http_build_query($data);
                $opts[CURLOPT_HTTPHEADER][] = 'Content-Length: ' . strlen($opts[CURLOPT_POSTFIELDS]);
                break;
            case 'DELETE':
                $opts[CURLOPT_CUSTOMREQUEST] = 'DELETE';
                break;
        }

        curl_setopt_array($ch, $opts);
        $resultBody = curl_exec($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $errno = curl_errno($ch);
        $error = curl_error($ch);

        curl_close($ch);

        if ($resultBody === false) {
            throw new \Exception(
                sprintf('Curl error: %s (%s)', $errno, $error)
            );
        }

        return json_decode($resultBody, true);
    }
}

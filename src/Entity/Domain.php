<?php
namespace ShopExpress\PowerDnsCrudApi\Entity;

use ShopExpress\PowerDnsCrudApi\ApiClient;

/**
 * Class Domain
 * @package ShopExpress\PowerDnsCrudApi\Entity
 */
class Domain implements \ArrayAccess
{
    /**
     * @var array
     */
    protected $container = array();  //список  полей

    /**
     * @var ApiClient
     */
    protected $apiClient;

    /**
     * @param ApiClient $apiClient The api client
     * @param array $row The array of preset values
     */
    public function __construct(ApiClient $apiClient, array $row = null)
    {
        $this->apiClient = $apiClient;

        if (is_array($row)) {
            $this->container = array_merge($this->container, $row);
        }
    }

    /**
     * Gets a Domain by name.
     *
     * @param string $domainName The Domain name
     *
     * @throws \Exception
     *
     * @return self
     */
    public function getByName($domainName)
    {
        $response = $this->apiClient->makeRequest(
            "{$this->apiClient->getBaseUrl()}/domains/name/$domainName",
            'GET'
        );

        if (isset($response['error'])) {
            throw new \Exception($response['error']['message'], 204);
        }

        if (json_last_error() != JSON_ERROR_NONE) {
            throw new \Exception(json_last_error_msg(), 204);
        }

        $this->apiClient->getLogger()->info('Domain successfully received!', [$domainName, $response]);

        // перезаписываем существующие значения на новые, если были указаны
        $this->container = array_merge($response[0], $this->container);

        return $this;
    }

    /**
     * Save a Domain if not exists by name or update else.
     *
     * @throws \Exception
     *
     * @return self
     */
    public function save()
    {
        if (!isset($this->container['name'])) {
            throw new \Exception("Domain name isn't set!", 400);
        }

        try {
            $this->getByName($this->container['name']);

            $response = $this->apiClient->makeRequest(
                "{$this->apiClient->getBaseUrl()}/domains/{$this->container['id']}",
                'PUT',
                $this->container
            );
        } catch (\Exception $e) {
            $response = $this->apiClient->makeRequest(
                "{$this->apiClient->getBaseUrl()}/domains",
                'POST',
                $this->container
            );
        }

        if (!isset($response['success'])) {
            throw new \Exception($response['error']['message'], 500);
        }

        $this->container['id'] = $response['success']['id'];

        $this->apiClient->getLogger()->info('Domain successfully saved!', [$this->container, $response]);

        return $this;
    }

    /**
     * Adds a RecordMx for Domain.
     *
     * @param array $data The data
     *
     * @throws \Exception
     * @return self
     */
    public function addRecordMx(array $data)
    {
        $data['type'] = 'MX';

        return $this->addRecord($data);
    }

    /**
     * Adds a record for Domain.
     *
     * @param array $data The data
     *
     * @throws \Exception
     * @return self
     */
    public function addRecord(array $data)
    {
        if (!isset($data['domain_id'])) {
            if (isset($this->container['id'])) {
                $data['domain_id'] = $this->container['id'];
            }
        }

        $this->apiClient->addRecord($data);

        return $this;
    }

    /**
     * @param $id
     * @param array $data
     *
     * @throws \Exception
     */
    public function updateRecord($id, array $data)
    {
        $this->apiClient->updateRecord($id, $data);
    }

    /**
     * @param $id
     *
     * @throws \Exception
     */
    public function deleteRecord($id)
    {
        $this->apiClient->deleteRecord($id);
    }

    /**
     * Set a value to container.
     *
     * @param mixed $offset The offset
     * @param mixed $value  The value
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Isset a value in container.
     *
     * @param mixed $offset The offset
     *
     * @return mixed
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Unset a value from container.
     *
     * @param mixed $offset The offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets a value from container.
     *
     * @param mixed $offset The offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }
}

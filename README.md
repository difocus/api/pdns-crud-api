Description of Power DNS CRUD API

```php

use ShopExpress\PowerDnsCrudApi\ApiClient;

$r = new ApiClient($config['BASE_URL'], $config['API_TOKEN']);

$d1 = $r->addDomain(['name' => 'difocus.ru', 'type' => 'MASTER']);
$d2 = $r->getDomain('epetrov.org')->addRecord(['name' => 'test', 'type' => 'AA'])->addRecordMx(['name' => 'test2']);

$r->addRecord(['domain_id' => 64, 'name' => 'test', 'type' => 'AAA']);

print_r([$d1, $d2]);
```